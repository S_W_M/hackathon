<?php

if(isset($_POST['first_name']) ) {

    include(__DIR__ . '/../vendor/joshcam/mysqli-database-class/MysqliDb.php');
    $db = new MysqliDb ('localhost', 'root', 'root', 'sb');

    $business_service = isset($_POST['business_service']) ? $_POST['business_service'] : 0;
    $company_id = isset($_POST['company_id']) ? $_POST['company_id'] : '';
    $service_id = isset($_POST['service_id']) ? $_POST['service_id'] : 4;
    $image = isset($_POST['image']) ? $_POST['image'] : '';
    $first_name = isset($_POST['first_name']) ? $_POST['first_name'] : '';
    $last_name = isset($_POST['last_name']) ? $_POST['last_name'] : '';
    $email = isset($_POST['email']) ? $_POST['email'] : '';
    $phone = isset($_POST['phone']) ? $_POST['phone'] : '';
    $new_user = isset($_POST['new_user']) ? $_POST['new_user'] : '';
    $duration = isset($_POST['duration']) ? $_POST['duration'] : '';

    $db->where ("email", $email, '=');
    $res = $db->get ("client");

    if($new_user == 1 && empty($res)) {

        $data = array(
            'firstname' => $first_name,
            'lastname' => $last_name,
            'email' => $email,
            'phone_number' => $phone,
            'image' => $image,
        );

        $id = $db->insert("client", $data);
    }

    $db->where ("email", $email, '=');
    $res = $db->get ("client");
//    $date = date('Y-m-d G:i:s', time());
//    $dateDur = date('Y-m-d G:i:s', time() + $duration);
    $data = array(
        'office_id' => $company_id,
        'client_id' => isset($res[0]['id']) ? $res[0]['id'] : '',
        'time_of_reservation' => date('Y-m-d G:i:s', time() + $duration),
        'time_of_arrival' => null,
        'service_id' => $service_id,
        'web_scheduled' => 1,
        'priority' => 3,
        'served' => 0,
        'is_being_served_now' => 0
    );

    $id = $db->insert ("queue", $data);
    echo json_encode($id);


}
