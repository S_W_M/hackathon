<?php
/**
 * Project: sb.local
 *
 * Created by vladicabibeskovic.
 * Date: 28.5.16., 01.49
 */

//echo 'terminal loading';

?>
<!-- delete after index -->
<script   src="https://code.jquery.com/jquery-1.12.4.min.js"   integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="   crossorigin="anonymous"></script>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<link rel="stylesheet" href="../demo/assets/font_awesome/css/font-awesome.min.css">

<div class="container terminal" style="padding-top: 40px">
    <div class="row">
        <div class="col-md-4 terminal_menu">
            <span class="message" style="display:block;background: #ff8100;"></span>
            <button type="button" class="btn btn-default web_broj">Zakazan Web Broj</button>
            <br/>
            <button type="button" class="btn btn-default fizicka_lica">Fizicka Lica</button>
            <br/>
            <button type="button" class="btn btn-default pravna_lica">Pravna Lica</button>
            <br/>
        </div>
        <div class="col-md-4 terminal_menu_back" style="display: none;">
            <button type="button" class="btn btn-default terminal_menu_back_btn"><i class="fa fa-backward" aria-hidden="true"></i> Nazad</button>
            <span type="button" class="btn btn-default terminal_menu_back_info"></span>
        </div>

        <div class="col-md-8 web_broj_options" style="display: none;">
            <input name="web_broj_value" id="confirm_desk_id" type="text" placeholder="Molimo Vas unesite Vas WEB broj" style="width: 40%;margin: 5px;text-align: center;border: 0;font-size: 16px;font-weight: 500;">
            <br/>
            <div class="col-sm-8 col-sm-offset-2 terminal_keypad" style="width: 400px; margin: 0 auto;">
                <button type="button" class="btn btn-default" action="1">1</button>
                <button type="button" class="btn btn-default" action="2">2</button>
                <button type="button" class="btn btn-default" action="3">3</button>
                <button type="button" class="btn btn-default" action="4">4</button>
                <button type="button" class="btn btn-default" action="5">5</button>
                <button type="button" class="btn btn-default" action="6">6</button>
                <button type="button" class="btn btn-default" action="7">7</button>
                <button type="button" class="btn btn-default" action="8">8</button>
                <button type="button" class="btn btn-default" action="9">9</button>
                <button type="button" class="btn btn-warning" action="del"><i class="fa fa-backward" aria-hidden="true" style="line-height: 20px;"></i></button>
                <button type="button" class="btn btn-default" action="0">0</button>
                <button type="button" class="btn btn-primary web_broj_confirm" action="ok">Unesi</button>
            </div>
        </div>
        <div class="col-md-8 fizicka_lica_options" style="display: none;">
            <button type="button" class="btn btn-default" data-service-id="3">Uplate / Isplate</button>
            <br/>
            <button type="button" class="btn btn-default" data-service-id="4">Otvaranje Racuna</button>
            <br/>
            <button type="button" class="btn btn-default" data-service-id="5">Krediti</button>
            <br/>
        </div>
        <div class="col-md-8 pravna_lica_options" style="display: none;">
            <button type="button" class="btn btn-default" data-service-id="1">Uplata Pazara</button>
            <br/>
            <button type="button" class="btn btn-default" data-service-id="2">Krediti</button>
            <br/>
        </div>
    </div>
</div>

<style>
    .terminal {
        text-align: center;
        padding-top: 10px;
    }
    .terminal .btn {
        margin: 5px;
        width: 200px;
    }
    .terminal_menu .btn {
        margin: 5px;
    }
    .terminal_keypad .btn {
        margin: 5px;
        width: 100px;
    }
    .terminal p {
        width: 100%;
        padding: 5px;
        background: #ff8100;
    }
</style>

<script type="text/javascript">
    $(document).ready(function(){
        $('.web_broj').click(function(){
            $('.pravna_lica_options').hide();
            $('.fizicka_lica_options').hide();
            $('.web_broj_options').show();
            $('.terminal_menu_back').show();
            $('.terminal_menu_back_info').html('Web kod').show();
            $('.terminal_menu').hide();
        });
        $('.fizicka_lica').click(function(){
            $('.pravna_lica_options').hide();
            $('.fizicka_lica_options').show();
            $('.web_broj_options').hide();
            $('.terminal_menu_back').show();
            $('.terminal_menu_back_info').html('Fizicka Lica').show();
            $('.terminal_menu').hide();
        });
        $('.pravna_lica').click(function(){
            $('.pravna_lica_options').show();
            $('.fizicka_lica_options').hide();
            $('.web_broj_options').hide();
            $('.terminal_menu_back').show();
            $('.terminal_menu_back_info').html('Pravna Lica').show();
            $('.terminal_menu').hide();
        });
        $('.terminal_menu_back_btn').click(function(){
            $('.pravna_lica_options').hide();
            $('.fizicka_lica_options').hide();
            $('.web_broj_options').hide();
            $('.terminal_menu_back').hide();
            $('.terminal_menu_back_info').hide();
            $('.terminal_menu').show();
        });
        $('.web_broj_confirm').click(function(){
            var web_broj = $('input[name="web_broj_value"]').val();
            var confirm_desk_id = $('#confirm_desk_id').val();
            $.ajax({
                url: "http://sb.local/terminal/confirm_number.php",
                data: {
                    "client_id": confirm_desk_id
                },
                cache: false,
                type: "POST",
                success: function(result){
                    var obj = jQuery.parseJSON( result );
                    if ( obj.status ) {
                        $('.terminal_menu_back_btn').click();
                        $('span.message').html('Vas broj je: ' + obj.number);
                        $('input[name="web_broj_value"]').val('');
                    } else {
                        $('.terminal_menu_back_btn').click();
                        $('span.message').html('Sifra ne postoji!');
                        $('input[name="web_broj_value"]').val('');
                    }
                }
            });

            console.log($('#google_map', window.parent.document).attr('src', $('#google_map', window.parent.document).attr('src')));
        });
        $('.fizicka_lica_options .btn').click(function(){
            var service = $(this).attr('data-service-id');
            $.ajax({
                url: "http://sb.local/terminal/confirm_number.php",
                data: {
                    "service": service,
                    "office_id": 1
                },
                cache: false,
                type: "POST",
                success: function(result){
                    var obj = jQuery.parseJSON( result );
                    if ( obj.status) {
                        $('.terminal_menu_back_btn').click();
                        $('span.message').html('Vas broj je: ' + obj.number);
                        $('input[name="web_broj_value"]').val('');
                    }
                }
            });
        });
        $('.pravna_lica_options .btn').click(function(){
            var service = $(this).attr('data-service-id');
            $.ajax({
                url: "http://sb.local/terminal/confirm_number.php",
                data: {
                    "service": service,
                    "office_id": 1
                },
                cache: false,
                type: "POST",
                success: function(result){
                    var obj = jQuery.parseJSON( result );
                    if ( obj.status ) {
                        $('.terminal_menu_back_btn').click();
                        $('span.message').html('Vas broj je: ' + obj.number);
                        $('input[name="web_broj_value"]').val('');
                    }
                }
            });
        });
        $('.terminal_keypad button').click(function(){
            var action = $(this).attr('action');
            if( action != 'del' && action != 'ok'){
                var old_val = $('input[name="web_broj_value"]').val();
                $('input[name="web_broj_value"]').val(old_val + action);
            } else if ( action == 'del' ) {
                var val = $('input[name="web_broj_value"]').val();
                var new_val = val.slice(0,-1);
                $('input[name="web_broj_value"]').val(new_val);
            }
        });
    });
</script>
