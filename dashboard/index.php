<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Dashboard</title>
    <link rel="shortcut icon" type="image/png" href="../demo/assets/favicon.ico"/>
    <!-- delete after index -->
    <script   src="https://code.jquery.com/jquery-1.12.4.min.js"   integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../demo/assets/font_awesome/css/font-awesome.min.css">
    <script   src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"   integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw="   crossorigin="anonymous"></script>
</head>
<body>
    <div class="row">
        <div class="col-md-10 col-md-offset-1" style="margin-top: 30px;">
            <button type="button" class="btn btn-default pull-right">
                <a href="http://sb.local/services" target="_blank">Services</a>
            </button>
        </div>
        <div class="col-md-10 col-md-offset-1 custom_css">
            <table id="sort" class="table table-bordered">
                <thead>
                <tr>
                    <td>Desk</td>
                    <td>Status</td>
                    <td>Klijent</td>
                    <td>Zahtev</td>
                    <td>Prioritet</td>
                    <td>Akcija</td>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>
    <style>
        .custom_css {
            border: solid 25px;
            border-radius: 11px;
            padding: 20px 20px 0 20px;
            margin-top: 30px;
        }
        body {
            background: #F3F3F3;
        }
        table {
            text-align: center;
            width: 100%;
        }
        thead tr {
            font-weight: 900;
        }
        tbody tr {
            cursor: pointer;
        }
        tbody tr:focus {
            cursor: move;
        }
        tr {
            background: #F6F6F6;
        }
        tr:nth-child(2n - 1) {
            background: #EFEFEF;
        }
        td {
            padding: 10px;
        }
    </style>
    <script>
        $(document).ready(function(){
            getQueue();
    //        setInterval(function() {
    //            getQueue();
    //        }, 5000);
        });

        function getQueue(){
            $.ajax({
                url: "http://sb.local/dashboard/current_queue.php",
                data: {
                    "office_id": 1
                },
                cache: false,
                type: "POST",
                dataType: "json",
                success: function (result) {
                    $('tbody').html('');
                    jQuery.each(result, function (index, value) {
                        var name = 'Klijent Nepoznat';
                        if(value["client_name"]){
                            name = value["client_name"];
                        }
                        var bussiness = 'Fizicko Lice';
                        if(value["bussiness"]){
                            bussiness = 'Pravno Lice';
                        }
                        var desk = '';
                        if(value["desk_id"]){
                            desk = '<button class="btn btn-xs btn-default">Desk '+value["desk_id"]+'</button> ';
                        }
                        $('tbody').append(
                            '<tr row_order="'+value["id"]+'" class="ui-state-default ui-sortable-handle">'+
                            '<td>'+value["office_name"]+'</td>'+
                            '<td>'+bussiness+'</td>'+
                            '<td>'+name+'</td>'+
                            '<td>'+value["service_name"]+'</td>'+
                            '<td>'+value["priority"]+'</td>'+
                            '<td>'+desk+'<button class="btn btn-xs btn-primary served_queue" queue_id="'+value["id"]+'">Zavrseno</button></td>'+
                            '</tr>'
                        );
                    });

                    var fixHelperModified = function (e, tr) {
                            var $originals = tr.children();
                            var $helper = tr.clone();
                            $helper.children().each(function (index) {
                                $(this).width($originals.eq(index).width())
                            });
                            return $helper;
                        },
                        updateIndex = function (e, ui) {
                            $('td.index', ui.item.parent()).each(function (i) {
                                $(this).html(i + 1);
                            });
                        };

                    $("#sort tbody").sortable({
                        helper: fixHelperModified,
                        stop: updateIndex,
                        update: function (event, ui) {
                            console.log('changed');
    //                        $.ajax({
    //                            url: "http://sb.local/point/current_queue.php",
    //                            data: {
    //                                "office_id": 1
    //                            },
    //                            cache: false,
    //                            type: "POST",
    //                            dataType: "json",
    //                            success: function(result){
    //                                jQuery.each(result, function( index, value ) {
    //                                    var el_class = (value.web_scheduled)? 'btn-primary' : 'btn-default';
    //                                    var element = '<button type="button" class="btn '+el_class+'">'+value.id+'</button>';
    //                                    $('.queue_next').append(element);
    //                                });
    //                            }
    //                        });
                        }
    //                    start: function(event, ui) {
    //                        console.log('started');
    //                    }
                    }).disableSelection();

                    $('.served_queue').on('click',function(){
                        var queue_id = $(this).attr('queue_id');
                        console.log(queue_id);
                        $.ajax({
                            url: "http://sb.local/dashboard/served_queue.php",
                            data: {
                                "queue_id": queue_id
                            },
                            cache: false,
                            type: "POST",
                            dataType: "json",
                            success: function(result){
                                getQueue();
                            }
                        });
                        
                    });
                }
            });
        }
    </script>
</body>
</html>