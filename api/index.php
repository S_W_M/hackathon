<?php
/**
 * Project: dtable
 *
 * Created by vladicabibeskovic.
 * Date: 28.5.16., 09.24
 */

echo json_encode(array(
    'request' => array(
        'request_url' => 'http://tweetbot.com',
        'client' => array(
            'client_id' => 432355,
            'client_name' => 'Petar Petrovic',
        ),
        'branch' => array(
            'name' => '',
            'city' => 'Beograd',
            'country' => 'Srbija',
            'address' => 'Bulevar Oslobodjenja 43',
            'time' => '30/05/2016 16:00:00',
            'appointment_id' => 433,
        )
    ),
    'generated_url' => 'http://sb.local/appointment/346',
));