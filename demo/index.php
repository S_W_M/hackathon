<?php
/**
 * Project: sb.local
 *
 * Created by vladicabibeskovic.
 * Date: 28.5.16., 01.54
 */

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>SC Demo</title>
    <link rel="shortcut icon" type="image/png" href="/demo/assets/favicon.ico"/>
    <link rel="stylesheet" href="assets/css/iphone.css">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://sb.local/demo/assets/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="http://sb.local/demo/assets/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!--Monitor -->
    <link rel="stylesheet" href="http://sb.local/demo/assets/css/style.css">
    <link rel="stylesheet" href="http://sb.local/demo/assets/css/macbook.css">
    <!-- Latest compiled and minified JavaScript -->
    <script src="http://sb.local/demo/assets/js/jquery.min.js"></script>
    <script src="http://sb.local/demo/assets/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script src="https://js.pusher.com/3.1/pusher.min.js"></script>
    <script>

        var pusher = new Pusher('bff980b17516951fe010', {
            cluster: 'eu',
            encrypted: true
        });

        var channel = pusher.subscribe('test_channel');
        channel.bind('my_event', function(data) {
            $('div.pusher_notification .notification_message').text(data.message);
            $('div.pusher_notification').show();
        });
    </script>
    <style>
        .demo .container {
            border: 1px solid #333;
            background-color: #eee;
            margin:10px;
            display: inline-block;
            width: 100%;
            padding: 0;
        }
        .container iframe {
            width: 100%;
        }
        .main-action {
            padding-top: 10px;
        }
        .phone_container {
            background-image: url('/demo/assets/img/phone.png');
            background-size: 100%;
            background-color: transparent;
            background-repeat: no-repeat;
            border: 0;
            padding: 76px 21px 0px;
            width: 330px;
        }
        .queue_next button {
            margin-right: 5px;
        }
    </style>
</head>
<body class="demo">
<div class="pusher_notification alert alert-success" style="display: none">
    <a href="#" class="close" onclick="$('.alert').hide()">&times;</a>
    <div class="notification_message"></div>
</div>
<div class="conainer">
    <div class="row">
        <div class="col-lg-12">
<!--            <div class="col-lg-12 main-action">-->
<!--                Actions: <button class="btn btn-xs btn-primary">Run</button>-->
<!--            </div>-->
            <div class="col-lg-4">
                <div class="wrapper">
                    <div class="iphone">
                        <div class="power"></div>
                        <div class="lock"></div>
                        <div class="volume up"></div>
                        <div class="volume down"></div>
                        <div class="camera"></div>
                        <div class="speaker"></div>
                        <div class="screen">
                            <div style="background: #fff; text-align: center; height: 50px; line-height: 50px; font-size: 20px; font-weight: 600;">
                                Izaberite ekspozituru
                            </div>
                        <iframe src="/google_map.php" style="width: 100%; height: 100%"></iframe>

                        </div>
                        <div class="button">
                            <div class="square"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-8" id="slider_div" style="position: absolute; height:100%; right:0; background-color: #FFFFFF; z-index:1">
                <button type="button" class="btn btn-primary" id="hide_button" style="margin-top: 50px; width: 100px">
                    <span>&rarr;</span>
                </button>
            </div>
            <div class="col-lg-8">
                <div class="monitor">
                    <div class="md-imac md-glare" style="height: 30em;">
                        <div class="md-body">
                            <div class="md-top">
                                <div class="monitor-display">
                                    <iframe src="http://sb.local/terminal" style="height: 100%; width: 100%"></iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12" style="padding: 20px 0 0 0">
                            <div class="col-lg-4">
                                <p style="font-weight: 600; font-size: 16px; margin-bottom: 0;">Šalter br. 1</p>
                                <div class="container">
                                    <iframe src="http://sb.local/point?id=1"></iframe>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <p style="font-weight: 600; font-size: 16px; margin-bottom: 0;">Šalter br. 2</p>
                                <div class="container">
                                    <iframe src="http://sb.local/point?id=2"></iframe>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <p style="font-weight: 600; font-size: 16px; margin-bottom: 0;">Šalter br. 3</p>
                                <div class="container">
                                    <iframe src="http://sb.local/point?id=3"></iframe>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <span>Sledeci na redu:</span>
                            <span class="queue_next">

                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12" style="margin-top: -35px; padding-left: 50px;">
                <button type="button" class="btn btn-default">
                    <a href="http://sb.local/dashboard" target="_blank">Dashboard</a>
                </button>
                <button type="button" class="btn btn-default">
                    <a href="http://sb.local/api" target="_blank">API</a>
                </button>
                <button type="button" class="btn btn-default">
                    <a href="http://sb.local/services" target="_blank">Services</a>
                </button>
            </div>
        </div>
    </div>
<script>
    $( "#hide_button" ).click(function() {
            $('#slider_div').hide('fast');
    });
    $( document ).ready(function() {
        refreshWaitingLine();
        setInterval('refreshWaitingLine()', 2000);
    });
    function refreshWaitingLine() {
        var pointId = $('#num').val();
        var current = $('.number').html();
        $.ajax({
            url: "http://sb.local/point/current_queue.php",
            data: {
                "office_id": 1
            },
            cache: false,
            type: "POST",
            dataType: "json",
            success: function(result){
                $('.queue_next').html('');
                jQuery.each(result, function( index, value ) {
                    var el_class = 'btn-default';
                    if( value.web_scheduled == 1 ){
                        el_class = 'btn-primary';
                    } else if (value.bussiness == 1 ){
                        el_class = 'btn-info';
                    }
                    var element = '<button type="button" class="btn '+el_class+'">'+value.id+'</button>';
                    $('.queue_next').append(element);
                });
            }});
        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
    }
</script>
</body>
</html>
