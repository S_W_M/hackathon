<?php
require($_SERVER['DOCUMENT_ROOT'] . '/lib/Pusher.php');

$options = array(
    'cluster' => 'eu',
    'encrypted' => true
);
$pusher = new Pusher(
    'bff980b17516951fe010',
    '67a79c79537129560813',
    '211395',
    $options
);

$data['message'] = $_POST['last_name'] . ' ' . $_POST['first_name'] . ' je zakazao dolazak za ' . round($_POST['duration'] / 60) . ' minuta!';
$pusher->trigger('test_channel', 'my_event', $data);

function mg_send($to, $subject, $message) {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, 'api:key-3ax6xnjp29jd6fds4gc373sgvjxteol0');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    $plain = strip_tags(nl2br($message));

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v2/samples.mailgun.org/messages');
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('from' => 'support@samples.mailgun.org',
        'to' => $to,
        'subject' => $subject,
        'html' => $message,
        'text' => $plain));

    $j = json_decode(curl_exec($ch));

    $info = curl_getinfo($ch);

    if($info['http_code'] != 200)
        error("Fel 313: Vänligen meddela detta via E-post till support@".DOMAIN);

    curl_close($ch);

    return $j;
}

mg_send('vlajko85@gmail.com', 'Hello', 'Testing some Mailgun awesomeness!');
