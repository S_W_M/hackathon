<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Project map</title>
    <style type="text/css">
        html, body { height: 100%; margin: 0; padding: 0; }
        #map { height: 100%; }
    </style>
    <!-- delete after index -->
    <script   src="https://code.jquery.com/jquery-1.12.4.min.js"   integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</head>
<body>
<div id="map"></div>

<!-- Modal HTML -->
<div id="myModalFirst" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Izaberite uslugu?</h5>
            </div>
            <div class="modal-body" style="text-align: center;">
                <!--<p>Do you want to save changes you made to document before closing?</p>-->
                <!--<p class="text-warning"><small>If you don't save, your changes will be lost.</small></p>-->
                <button type="button" onclick="changeClassOfElement('new-user', this);" class="btn-sm btn-default button-options new-user" data-new-user="1" data-target="#myModalThird" data-toggle="modal" data-dismiss="modal" style="margin-top: 0px;">Otvaranje Racuna</button>
                <button type="button" onclick="changeClassOfElement('new-user', this);" class="btn-sm btn-default button-options new-user" data-new-user="0" data-target="#myModalSecond" data-toggle="modal" data-dismiss="modal" style="margin-top: 10px;">Postojeci Korisnik</button>
            </div>
            <div class="modal-footer">
                <!--<button type="button" class="btn btn-default" data-dismiss="modal">Zatvori</button>-->
                <!--<button type="button" class="btn-sm btn-primary" data-target="#myModalSecond" data-toggle="modal" data-dismiss="modal">Dalje</button>-->
            </div>
        </div>
    </div>
</div>

<!-- Modal HTML -->
<div id="myModalSecond" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h5 class="modal-title">Izaberite uslugu?</h5>
            </div>
            <div class="modal-body" style="text-align: center;">
                <!--<p>Do you want to save changes you made to document before closing?</p>-->
                <!--<p class="text-warning"><small>If you don't save, your changes will be lost.</small></p>-->
                <button onclick="changeClassOfElement('business-service', this);" type="button" class="btn-sm btn-default fizicka_lica button-options business-service" data-business-service="1" style="margin-top: 0px;">Fizicka Lica</button>
                <button onclick="changeClassOfElement('business-service', this);" type="button" class="btn-sm btn-default pravna_lica button-options business-service" data-business-service="0" style="margin-top: 10px;">Pravna Lica</button>

                <div class="fizicka_lica_options" style="display: none; margin-top: 10px; border-top: 1px solid gray;">
                    <button onclick="changeClassOfElement('service-button', this);" type="button" class="btn-xs btn-default button-options service-button" data-service-id="3" data-target="#myModalThird" data-toggle="modal" data-dismiss="modal">Uplate / Isplate</button>
                    <button onclick="changeClassOfElement('service-button', this);" type="button" class="btn-xs btn-default button-options service-button" data-service-id="4" data-target="#myModalThird" data-toggle="modal" data-dismiss="modal">Otvaranje Racuna</button>
                    <button onclick="changeClassOfElement('service-button', this);" type="button" class="btn-xs btn-default button-options service-button" data-service-id="5" data-target="#myModalThird" data-toggle="modal" data-dismiss="modal">Krediti</button>
                </div>
                <div class="pravna_lica_options" style="display: none; margin-top: 10px; border-top: 1px solid gray;">
                    <button onclick="changeClassOfElement('service-button', this);" type="button" class="btn-xs btn-default button-options service-button" data-service-id="1" data-target="#myModalThird" data-toggle="modal" data-dismiss="modal">Uplata Pazara</button>
                    <button onclick="changeClassOfElement('service-button', this);" type="button" class="btn-xs btn-default button-options service-button" data-service-id="2" data-target="#myModalThird" data-toggle="modal" data-dismiss="modal">Krediti</button>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-sm btn-default" data-target="#myModalFirst" data-toggle="modal" data-dismiss="modal">Prethodno</button>
                <!--<button type="button" class="btn-sm btn-primary" data-target="#myModalThird" data-toggle="modal" data-dismiss="modal">Dalje</button>-->
            </div>
        </div>
    </div>
</div>

<!-- New Modal HTML -->
<div id="myModalThird" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title modal-title-third">Ulogovani ste kao:</h4>
            </div>
            <div class="modal-body" style="text-align: center;">
                <p><fb:login-button scope="public_profile,email" onlogin="checkLoginState();" id="loginBtn" data-size="xlarge" style="display:none;"></fb:login-button></p>
                <!--<p class="text-warning"><small>qweqwe</small></p>-->
                <div><div class="fb-image"></div><div class="fb-name-get"></div></div>
                <input placeholder="Broj telefona" type="text" class="phone" style="margin-top:10px;">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-sm btn-default" data-target="#myModalFirst" data-toggle="modal" data-dismiss="modal">Prethodno</button>
                <button type="button" class="btn-sm btn-default submit-button" data-dismiss="modal" data-toggle="modal" data-target="#myModalFourth" onclick="submitData();">Posalji Zahtev</button>
                <!--<button type="button" class="btn btn-default" data-target="#myModal" data-toggle="modal" data-dismiss="modal">Back</button>-->
                <!--<button type="button" class="btn btn-primary" >Save changes</button>-->
            </div>
        </div>
    </div>
</div>

<!-- New Modal HTML -->
<div id="myModalFourth" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title modal-title-fourth">Vas web broj za terminal:</h4>
            </div>
            <div class="modal-body" style="text-align: center;">
                <!--<p class="text-warning"><small>qweqwe</small></p>-->
                <div class="queue_id" style="font-size: 35px;color: #01ff5f;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn-sm btn-default" data-dismiss="modal">Zatvori</button>
                <!--<button type="button" class="btn btn-default" data-target="#myModal" data-toggle="modal" data-dismiss="modal">Back</button>-->
                <!--<button type="button" class="btn btn-primary" >Save changes</button>-->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var initPoint = {lat: 44.805845, lng: 20.5065371};
    var initialLocation;

    var points = [
        //['You are here', 44.805845, 20.5065371,''],
        ['Branch 1', 44.805736, 20.509510, 3, 1],
        ['Branch 2', 44.808997, 20.498552, 25, 1],
        ['Branch 3', 44.795306, 20.509391, 10, 1],
    ];

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            scrollwheel: false,
            zoom: 14
        });

        if (navigator.geolocation) {
            browserSupportFlag = true;
            navigator.geolocation.getCurrentPosition(function (position) {
                initialLocation = [position.coords.latitude, position.coords.longitude];
                map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));

                var marker = new google.maps.Marker({
                    position: {lat: position.coords.latitude, lng: position.coords.longitude},
                    map: map,
                    icon: new google.maps.MarkerImage('http://sb.local/demo/assets/img/map-marker-with-a-person-shape.svg',
                        null, null, null, new google.maps.Size(40,40))
                });

                $.each(points, function (i, val) {
                    var distanceService = new google.maps.DistanceMatrixService;
                    var directionsService = new google.maps.DirectionsService;
                    var directionsDisplay = new google.maps.DirectionsRenderer({
                        markerOptions: {visible: false},
                        preserveViewport: true

                    });
                    calculateDistance(distanceService, initialLocation, points[i], i, map);
                    calculateAndDisplayRoute(directionsService, directionsDisplay, initialLocation, points[i]);
                    directionsDisplay.setMap(map)
                })
            }, function () {
                handleNoGeolocation(browserSupportFlag);
            });
        } else {
            browserSupportFlag = false;
            handleNoGeolocation(browserSupportFlag);
        }
    }

    function handleNoGeolocation(errorFlag) {
        var map = new google.maps.Map(document.getElementById('map'), {
            scrollwheel: false,
            zoom: 14
        });
        if (errorFlag) {
            var content = 'Error: The Geolocation service failed.';
        } else {
            var content = 'Error: Your browser doesn\'t support geolocation.';
        }

        initialLocation = [44.805845, 20.5065371];
        map.setCenter(new google.maps.LatLng(44.805845, 20.5065371));
        var marker = new google.maps.Marker({
            position: {lat: 44.805845, lng: 20.5065371},
            map: map,
            icon: new google.maps.MarkerImage('http://sb.local/demo/assets/img/map-marker-with-a-person-shape.svg',
                null, null, null, new google.maps.Size(40,40)),
        });

        $.each(points, function (i, val) {
            var distanceService = new google.maps.DistanceMatrixService;
            var directionsService = new google.maps.DirectionsService;
            var directionsDisplay = new google.maps.DirectionsRenderer({
                markerOptions: {visible: false},
                preserveViewport: true

            });
            calculateDistance(distanceService, initialLocation, points[i], i, map);
            calculateAndDisplayRoute(directionsService, directionsDisplay, initialLocation, points[i]);
            directionsDisplay.setMap(map);
        })
    }

    function calculateDistance(distanceService, origin, destination, index, map) {
        distanceService.getDistanceMatrix({
            origins: [{lat: origin[0], lng: origin[1]}],
            destinations: [{lat: destination[1], lng: destination[2]}],
            travelMode: google.maps.TravelMode.WALKING
        }, function(response, status) {
            if (status !== google.maps.DistanceMatrixStatus.OK) {
                alert('Error was: ' + status);
            } else {
                points[index].push(response.rows[0].elements[0].distance.text);
                points[index].push(response.rows[0].elements[0].duration.text);
                points[index].push(response.rows[0].elements[0].duration.value);

                var point = points[index];
                var progress_bar_class = 'progress-bar-success';

                if ( point[3] > 20 ) {
                    progress_bar_class = 'progress-bar-danger';
                } else if ( point[3] > 5 ) {
                    progress_bar_class = 'progress-bar-info';
                }

                var marker = new google.maps.Marker({
                    position: {lat: point[1], lng: point[2]},
                    map: map,
                    title: ( typeof distance != 'undefined' ) ? point[0] + ', Distance: ' + distance : point[0],
                    content: point[5] + ' (~' + point[6] + ')<br /><div class="progress" style="width: 100px; margin-bottom: 0; height: 16px;"> <button onclick="changeClassOfElement(\'progress-bar\', this);" data-target="#myModalFirst" data-toggle="modal" data-path-duration="'+point[7]+'" data-office-id="'+point[4]+'" class="progress-bar ' + progress_bar_class + '" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="min-width: 100%; line-height: 13px; border: 0">' + point[3] + ' min</button></div>',
                    icon: new google.maps.MarkerImage('http://sb.local/demo/assets/img/bank_marker.svg',
                        null, null, null, new google.maps.Size(40,40))
                });

                var infowindow = new google.maps.InfoWindow();
                infowindow.setContent(marker.content);
                infowindow.open(map, marker);

                google.maps.event.addListener(marker, 'click', function () {
                    infowindow.setContent(this.content);
                    infowindow.open(map, this);
                })
            }
        });
    }

    function calculateAndDisplayRoute(directionsService, directionsDisplay, origin, destination) {
        directionsService.route({
            origin: {lat: origin[0], lng: origin[1]},
            destination: {lat: destination[1], lng: destination[2]},
            travelMode: google.maps.TravelMode.WALKING
        }, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }

    function submitData() {
        var company_id = $('.progress-bar.active').attr("data-office-id");
        var duration = $('.progress-bar.active').attr("data-path-duration");
        var business_service = $('.business-service.active').attr("data-business-service");
        var service_id = $('.service-button.active').attr("data-service-id");
        var image = $('.image').val();
        var first_name = $('.first_name').val();
        var last_name = $('.last_name').val();
        var email = $('.email').val();
        var phone = $('.phone').val();
        var new_user = $('.new-user.active').attr("data-new-user");

        console.log(new_user);

        $.ajax({
            url: "http://sb.local/dashboard/add_data.php",
            data: {
                company_id: company_id,
                business_service: business_service,
                service_id: service_id,
                image: image,
                first_name: first_name,
                last_name: last_name,
                email: email,
                phone: phone,
                new_user: new_user,
                duration: duration,
            },
            cache: false,
            type: "POST",
            success: function(result){
                var res = $.parseJSON(result);
                console.log(res);
                if(res != false) {
                    $('.queue_id').html(res);

                    $.ajax({
                        url: "/demo/pusher_server.php",
                        type: "POST",
                        dataType: "json",
                        data: {
                            image: image,
                            first_name: first_name,
                            last_name: last_name,
                            duration: duration
                        }
                    });
                }
            }
        });
    }

    function changeClassOfElement(className, element) {
        $('.' + className).removeClass('active');
        $(element).addClass('active');
    }

</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB84me5xHtV3q-U6KKQDufDm_FAP5mPCEE&callback=initMap">
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('.web_broj').click(function(){
            $('.pravna_lica_options').hide();
            $('.fizicka_lica_options').hide();
            $('.web_broj_options').show();
            $('.terminal_menu_back').show();
            $('.terminal_menu_back_info').html('Web kod').show();
            $('.terminal_menu').hide();
        });
        $('.fizicka_lica').click(function(){
            $('.pravna_lica_options').hide();
            $('.fizicka_lica_options').show();
            $('.web_broj_options').hide();
            $('.terminal_menu_back').show();
            $('.terminal_menu_back_info').html('Fizicka Lica').show();
            $('.terminal_menu').hide();
        });
        $('.pravna_lica').click(function(){
            $('.pravna_lica_options').show();
            $('.fizicka_lica_options').hide();
            $('.web_broj_options').hide();
            $('.terminal_menu_back').show();
            $('.terminal_menu_back_info').html('Pravna Lica').show();
            $('.terminal_menu').hide();
        });
        $('.terminal_menu_back_btn').click(function(){
            $('.pravna_lica_options').hide();
            $('.fizicka_lica_options').hide();
            $('.web_broj_options').hide();
            $('.terminal_menu_back').hide();
            $('.terminal_menu_back_info').hide();
            $('.terminal_menu').show();
        });
        $('.web_broj_confirm').click(function(){
            var web_broj = $('input[name="web_broj_value"]').val();
            var confirm_desk_id = $('#confirm_desk_id').val();
            $.ajax({
                url: "http://sb.local/terminal/confirm_number.php",
                data: {
                    "client_id": confirm_desk_id
                },
                cache: false,
                type: "POST",
                success: function(result){
                    $('.terminal_menu_back_btn').click();
                    $('span.message').html('Vas broj je: ' + web_broj);
                    $('input[name="web_broj_value"]').val('');
                }
            });
            console.log('sent');
        });
        $('.terminal_keypad button').click(function(){
            var action = $(this).attr('action');
            if( action != 'del' && action != 'ok'){
                var old_val = $('input[name="web_broj_value"]').val();
                $('input[name="web_broj_value"]').val(old_val + action);
            } else if ( action == 'del' ) {
                var val = $('input[name="web_broj_value"]').val();
                var new_val = val.slice(0,-1);
                $('input[name="web_broj_value"]').val(new_val);
            }
        });
    });
</script>
<!-- Facebook login JavaScript-->
<script>
    // This is called with the results from from FB.getLoginStatus().
    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        // The response object is returned with a status field that lets the
        // app know the current login status of the person.
        // Full docs on the response object can be found in the documentation
        // for FB.getLoginStatus().
        if (response.status === 'connected') {
            $('#loginBtn').hide();
            $('.modal-title-third').html('Ulogovani ste kao:');
            $('.submit-button').prop('disabled', false);

            // Logged into your app and Facebook.
            getUserAPIData();
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            $('#loginBtn').show();
            $('.modal-title-third').html('Molimo ulogujte se');
            $('.submit-button').prop('disabled', true);
//            document.getElementById('status').innerHTML = 'Please log ' +
//                'into this app.';
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            $('#loginBtn').show();
            $('.modal-title-third').html('Molimo ulogujte se');
            $('.submit-button').prop('disabled', true);
//            document.getElementById('status').innerHTML = 'Please log ' +
//                'into Facebook.';
        }
    }

    // This function is called when someone finishes with the Login
    // Button.  See the onlogin handler attached to it in the sample
    // code below.
    function checkLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '612130778952447',
            cookie     : true,  // enable cookies to allow the server to access
                                // the session
            xfbml      : true,  // parse social plugins on this page
            version    : 'v2.5' // use graph api version 2.5
        });

        // Now that we've initialized the JavaScript SDK, we call
        // FB.getLoginStatus().  This function gets the state of the
        // person visiting this page and can return one of three states to
        // the callback you provide.  They can be:
        //
        // 1. Logged into your app ('connected')
        // 2. Logged into Facebook, but not your app ('not_authorized')
        // 3. Not logged into Facebook and can't tell if they are logged into
        //    your app or not.
        //
        // These three cases are handled in the callback function.

        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });

    };

    // Load the SDK asynchronously
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function getUserAPIData() {
//        console.log('Welcome!  Fetching your information.... ');
//        FB.api('/me', function(response) {
////            console.log(response);
////            console.log('Successful login for: ' + response.name);
//            $('#status').val(response.name);
////            document.getElementById('status').innerHTML =
////                'Thanks for logging in, ' + response.name + '!';
//        });
        FB.api('/me', {fields: 'first_name, last_name, picture, email,age_range, name'}, function(response) {
            $('.fb-image').append('<img src="'+response.picture.data.url+'">');
            $('.fb-name-get').append(response.name);
            $('.first_name').val(response.first_name);
            $('.last_name').val(response.last_name);
            $('.image').val(response.picture.data.url);
            $('.email').val(response.email);

        });
    }
</script>
<style>
    .button-options {
        display: block;
        margin: 0 auto;
        margin-top: 5px;
    }
</style>
<input type="hidden" name="first_name" class="first_name" value="" />
<input type="hidden" name="last_name" class="last_name" value="" />
<input type="hidden" name="image" class="image" value="" />
<input type="hidden" name="email" class="email" value="" />
<input type="hidden" name="phone" class="phone" value="" />
</body>
</html>