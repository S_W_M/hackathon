<?php
/**
 * Project: sb.local
 *
 * Created by vladicabibeskovic.
 * Date: 28.5.16., 02.44
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

$officeId = $_REQUEST['office_id'];

include(__DIR__ . '/../vendor/joshcam/mysqli-database-class/MysqliDb.php');
$db = new MysqliDb ('localhost', 'root', 'root', 'sb');

$desk = $db->rawQuery('
SELECT queue.id as id, queue.web_scheduled as web_scheduled, service.busness_service as bussiness  
from queue
inner join service on service.id = queue.service_id 
where queue.served = 0
  and queue.is_being_served_now = 0
  and queue.time_of_arrival is not NULL 
  and queue.office_id = ?
ORDER BY queue.id ASC
', Array ($officeId));


echo json_encode($desk);