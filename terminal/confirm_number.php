<?php
/**
 * Project: dtable
 *
 * Created by vladicabibeskovic.
 * Date: 28.5.16., 06.33
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

if( $_REQUEST['client_id'] ) {
    $clientId = $_REQUEST['client_id'];
    include(__DIR__ . '/../vendor/joshcam/mysqli-database-class/MysqliDb.php');
    $db = new MysqliDb ('localhost', 'root', 'root', 'sb');

    $user = $db->rawQuery('
        SELECT *  
        from client
        inner join queue on queue.client_id = client.id 
        where queue.id = ?
        ORDER BY queue.id ASC
        ', Array ($clientId));
    if (!$user) {
        $response = array(
            'status' => false
        );
        echo json_encode($response);
        exit();
    }
    $data = Array(
        'priority' => 1,
        'time_of_arrival' => date("Y-m-d H:i:s"),
        'is_being_served_now' => 0
    );
    $db->where('id', $clientId);

    if ($db->update('queue', $data)) {
        $response = array(
            'status'=>true,
            'number'=>$clientId
        );
        echo json_encode($response);
    } else {
        $response = array(
            'status'=>false
        );
        echo json_encode($response);
    }
} else {
    $service = $_REQUEST['service'];
    $office_id = $_REQUEST['office_id'];

    include(__DIR__ . '/../vendor/joshcam/mysqli-database-class/MysqliDb.php');
    $db = new MysqliDb ('localhost', 'root', 'root', 'sb');
    $data = Array(
        'office_id'     => $office_id,
        'client_id'       => 0,
        'time_of_reservation'   => date("Y-m-d H:i:s"),
        'time_of_arrival'       => date("Y-m-d H:i:s"),
        'service_id'    => $service,
        'priority'      => 4,
        'served'        => 0,
        'is_being_served_now'   => 0
    );
    $res = $db->insert ('queue', $data);
    if( $res ){
        $response = array(
            'status'=>true,
            'number'=>$res
        );
        echo json_encode($response);
    } else {
        $response = array(
            'status'=>false
        );
        echo json_encode($response);
    }
}




//var_dump($desk);