<?php
/**
 * Project: sb.local
 *
 * Created by vladicabibeskovic.
 * Date: 28.5.16., 02.44
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

$officeId = $_REQUEST['office_id'];

include(__DIR__ . '/../vendor/joshcam/mysqli-database-class/MysqliDb.php');
$db = new MysqliDb ('localhost', 'root', 'root', 'sb');

$desk = $db->rawQuery('
SELECT queue.id as id, queue.priority as priority,  queue.desk_id as desk_id, 
       service.name as service_name, service.busness_service as bussiness, 
       CONCAT(client.firstname," ", client.lastname) as client_name,
       office.name as office_name
from queue
left join service on queue.service_id = service.id
left join client on queue.client_id = client.id
left join office on office.id = queue.office_id
where queue.served = 0
  and queue.office_id = ?
  and queue.time_of_arrival IS NOT NULL
ORDER BY -queue.desk_id DESC
', Array ($officeId));


echo json_encode($desk);