<?php ?>
<!DOCTYPE html>
<html>
<head>
    <style>
        .number {
            font-size: 80.0vh;
            text-align: center;
            font-family: Arial;
            display: inline-block;
        }
    </style>
</head>
<body style="text-align: center;">
<input type="hidden" id="num" value="<?php echo (isset($_REQUEST['id']))?$_REQUEST['id'] : 1; ?>">
<div class="number">...</div>
<script src="http://sb.local/demo/assets/js/jquery.min.js"></script>
<script>
    $( document ).ready(function() {
        refreshPoint();
        setInterval('refreshPoint()', 2000);
    });
    function refreshPoint() {
        var pointId = $('#num').val();
        var current = $('.number').html();
        $.ajax({
            url: "http://sb.local/point/refresh.php",
            data: {
                "desk_id": pointId
            },
            cache: false,
            type: "POST",
            success: function(result){
            if( current != result && isNumeric(result)) {
                $('.number').html(result);
            } else if(result == '') {
                $('.number').html('<p style="font-size: 14px;">Šalter je slobodan molimo Vas uzmite svoj broj.</p>');
            }
        }});
        function isNumeric(n) {
            return !isNaN(parseFloat(n)) && isFinite(n);
        }
    }
</script>
</body>
</html>

