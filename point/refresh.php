<?php
/**
 * Project: sb.local
 *
 * Created by vladicabibeskovic.
 * Date: 28.5.16., 02.44
 */
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$deskId = $_REQUEST['desk_id'];

include(__DIR__ . '/../vendor/joshcam/mysqli-database-class/MysqliDb.php');

$db = new MysqliDb ('localhost', 'root', 'root', 'sb');


$desk = $db->rawQuery('
SELECT * from queue
where desk_id = ?
  and is_being_served_now = 1
  and served = 0
  and time_of_arrival IS NOT NULL
  LIMIT 1
', Array ($deskId));

if(isset($desk[0]['id'])) {
    echo $desk[0]['id'];
} else {
    // Get new item from the queue
    $desk = $db->rawQuery('
        UPDATE `queue` SET `desk_id`=?,`is_being_served_now`=1
          WHERE served = 0 AND is_being_served_now = 0 ORDER BY priority DESC, time_of_arrival ASC LIMIT 1', Array ($deskId));
}