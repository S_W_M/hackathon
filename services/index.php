<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Services</title>
    <script   src="https://code.jquery.com/jquery-1.12.4.min.js"   integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../demo/assets/font_awesome/css/font-awesome.min.css">
    <script   src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"   integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw="   crossorigin="anonymous"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.10.0/js/bootstrap-select.min.js"></script>
    <style type="text/css">
        html, body { height: 100%; margin: 0; padding: 0; }
        h1 { margin: 20px 0; font-size: 28px }
        #wrapper { width: 900px; margin: 0 auto; }
        .panel-title { overflow: hidden }
        .panel-title .section-title { margin-right: 20px; line-height: 34px }
        .panel-title .label {  }
        .panel-title .btn { float: right }
        .document .fa { font-size: 60px }
        .confirm { font-size: 16px; position: relative; }
        .confirm .fa-check-square-o { color: #2aa22a; }
        .display_options_wrapper .document, .display_options_wrapper .confirm, .display_options_wrapper .crm { border: 1px solid #999999; padding: 10px }
        .display_options_wrapper .form-control { float: left; width: 220px; margin-right: 10px }
        .display_options_wrapper .document, .display_options_wrapper .crm, .display_options_wrapper .confirm { margin-bottom: 10px; overflow: hidden }
        .bootstrap-select { margin-bottom: 10px }
    </style>
</head>
<body>
<div id="wrapper">
    <h1>Services</h1>
    <div class="panel-group" id="accordion">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <span class="section-title">Uplata Pazara</span> <span class="label label-warning">Business</span> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="collapse" aria-haspopup="true" aria-expanded="false" data-parent="#accordion" href="#collapse1">
                        Automate <span class="caret"></span>
                    </button>
                </h4>
            </div>
            <div id="collapse1" class="panel-collapse collapse">
                <div class="panel-body"></div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <span class="section-title">Krediti</span> <span class="label label-warning">Business</span> <button type="button" class="btn btn-default dropdown-toggle" data-toggle="collapse" aria-haspopup="true" aria-expanded="false" data-parent="#accordion" href="#collapse2">
                        Automate <span class="caret"></span>
                    </button>
                </h4>
            </div>
            <div id="collapse2" class="panel-collapse collapse">
                <div class="panel-body"></div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <span class="section-title">Uplate / Isplate</span><button type="button" class="btn btn-default dropdown-toggle" data-toggle="collapse" aria-haspopup="true" aria-expanded="false" data-parent="#accordion" href="#collapse3">
                        Automate <span class="caret"></span>
                    </button>
                </h4>
            </div>
            <div id="collapse3" class="panel-collapse collapse">
                <div class="panel-body"></div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <span class="section-title">Otvaranje Racuna</span><button type="button" class="btn btn-default dropdown-toggle" data-toggle="collapse" aria-haspopup="true" aria-expanded="false" data-parent="#accordion" href="#collapse4">
                        Automate <span class="caret"></span>
                    </button>
                </h4>
            </div>
            <div id="collapse4" class="panel-collapse collapse">
                <div class="panel-body"></div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <span class="section-title">Krediti</span><button type="button" class="btn btn-default dropdown-toggle" data-toggle="collapse" aria-haspopup="true" aria-expanded="false" data-parent="#accordion" href="#collapse5">
                        Automate <span class="caret"></span>
                    </button>
                </h4>
            </div>
            <div id="collapse5" class="panel-collapse collapse">
                <div class="panel-body"></div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <span class="section-title">Agent Ekspoziture</span><button type="button" class="btn btn-default dropdown-toggle" data-toggle="collapse" aria-haspopup="true" aria-expanded="false" data-parent="#accordion" href="#collapse6">
                        Automate <span class="caret"></span>
                    </button>
                </h4>
            </div>
            <div id="collapse6" class="panel-collapse collapse">
                <div class="panel-body"></div>
            </div>
        </div>
    </div>
    <div class="cloning" style="display: none">
        <select multiple title="Izbirite proizvod...">
            <option value="document">Štampanje dokumenta</option>
            <option value="crm">Povezivanje sa CRM</option>
            <option value="link">Slanje linka za ocenu iskustva</option>
            <option value="sms">Slanje SMS klijentu</option>
            <option value="email">Slanje emaila</option>
            <option value="push">Slanje PUSH obaveštenja</option>
        </select>
        <div class="document" data-position="0">
            <a href="#" class="close remove-document" data-position="0" data-dismiss="document" aria-label="close" title="close">×</a>
            <i class="fa fa-file-word-o" aria-hidden="true"></i><br />file.docx
        </div>
        <div class="crm"><a href="#" class="close remove-document"  data-position="1" data-dismiss="document" aria-label="close" title="close">×</a><input type="text" class="form-control" placeholder="API ID"><input type="text" class="form-control" placeholder="API SE"></div>
        <div class="confirm link"><a href="#" class="close remove-document"  data-position="2" data-dismiss="document" aria-label="close" title="close">×</a><i class="fa fa-check-square-o" aria-hidden="true"></i> Slanje linka za ocenu iskustva</div>
        <div class="confirm sms"><a href="#" class="close remove-document"  data-position="3"  data-dismiss="document" aria-label="close" title="close">×</a><i class="fa fa-check-square-o" aria-hidden="true"></i> Slanje SMS klijentu</div>
        <div class="confirm email"><a href="#" class="close remove-document"  data-position="4" data-dismiss="document" aria-label="close" title="close">×</a><i class="fa fa-check-square-o" aria-hidden="true"></i> Slanje emaila</div>
        <div class="confirm push"><a href="#" class="close remove-document"  data-position="5" data-dismiss="document" aria-label="close" title="close">×</a><i class="fa fa-check-square-o" aria-hidden="true"></i> Slanje PUSH obaveštenja</div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.panel-body').each(function (i, val) {
            $('.cloning select').clone().appendTo($(this)).addClass('selectpicker').removeClass('cloning').show();
            $(this).append('<div class="display_options_wrapper"></div>');
        })
        $('.cloning select').remove();

        $('.selectpicker').on('changed.bs.select', function (e) {
            var selected_options = e.target.selectedOptions;
            var display_options_wrapper = $(this).closest('.panel-body').find('.display_options_wrapper').html('');

            $.each(selected_options, function (i, val) {
                var option_html = $('.cloning .' + val.value).clone();
                display_options_wrapper.append(option_html);
                console.log(val.value);
            })

        });

        $(document).on('click', '.remove-document', function () {
            var position = $(this).data('position');
            $(this).closest('.display_options_wrapper').prev().find("[data-original-index='" + position + "']").children('a').click();
            $(this).parent().hide('fast').remove();
        });
    })
</script>
</body>
</html>