<?php
/**
 * Project: sb.local
 *
 * Created by vladicabibeskovic.
 * Date: 28.5.16., 02.44
 */
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);

$queue_id = $_REQUEST['queue_id'];

include(__DIR__ . '/../vendor/joshcam/mysqli-database-class/MysqliDb.php');
$db = new MysqliDb ('localhost', 'root', 'root', 'sb');
$queue = $db->rawQuery('
    UPDATE queue 
    SET is_being_served_now = 0, served = 1 
    WHERE id = ?',
    Array ($queue_id));

$desks = $db->rawQuery('
SELECT desk_id
from queue
where is_being_served_now = 1
');
$busy_desks = [];
foreach($desks as $item){
    $busy_desks[] = $item['desk_id'];
}
$allDesks = array(1,2,3);
foreach( $allDesks as $i ){
    if( !in_array($i,$busy_desks)){
        $desk = $i;
    }
}

$new_served_queue = $db->rawQuery('
    UPDATE queue
    SET is_being_served_now = 1, desk_id = ?
    WHERE served = 0
    AND is_being_served_now = 0
    AND time_of_arrival IS NOT NULL
    LIMIT 1',
    Array ($desk));




echo json_encode($queue);